import java.io.Closeable;
import java.io.IOException;

public class PessoaFisica extends Pessoa implements Reportavel,Closeable {

	private String rg;
	@Override
	//Metodo implementado para a interface reportavel
	public void Reportar() {
		System.out.println("O metodo deve ser implementado aqui");
		
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public final String apresentar() {
		return super.apresentar() + ", RG: " + rg;
	
	}

}
