
public class Pessoa {
	//Atributos
		public String nome;
		public int id;
		//Métodos
		public String nomeDaPessoa() {
			return "Olá, meu nome é " + nome;
		}
		//Construtores
		//Construtor 1 (vazio)
		public Pessoa() {
		}
		//Construtor 2 (sobrecarga)
		public Pessoa(String nome, int id) {
			this.nome = nome;
			this.id = id;
		}
		
		public String apresentar() {
			return "Nome: " +  nome;
		}



}
